package Autostand::main;
use strict;
use warnings;
use v5.10;
use utf8;
binmode(STDOUT,':utf8');

use File::Path;
use XML::Simple;
use Data::Dumper;
use Time::Piece;
use Time::Seconds;
use Log::Log4perl;
use Exporter qw(import);
use Autostand::dbworker qw(workWithTable);

Log::Log4perl::init('/home/user/perl/log4perl.conf');

my $logger = Log::Log4perl->get_logger('autostand');

our @EXPORT = qw(parseReportAndAddToDB);

sub parseReportAndAddToDB {
    my ($path, $specialKey) = @_;
    $logger->info("Got path $path and key $specialKey");
    my $ip = "/192.168.165.37";
    my $localDir = "autostand";

    #
    #Replace absolute path to local path, and check if report exist.
    #
    my $pathToParse = $path; 
    $pathToParse =~ s/$ip/$localDir/g;
    if(!(-e $pathToParse)){
        $logger->error("Report file didn't exist. $pathToParse");
        my %result = %{parsePath($path, $specialKey)};
        my $TABLE_FAILED_SCEN = "\"date\", \"platform\",\"scenario\"";
        my $VALUE_FAILED_SCEN = "to_date(\'$result{'date'}\', \'DD_MM_YY\'), \'$result{'platform'}\', \'$result{'scenario'}\'";
        workWithTable( "insert", "FailedScenarios", $TABLE_FAILED_SCEN, $VALUE_FAILED_SCEN );
        return;
    }

    #
    #Replace absolute path to relative, before adding to db.
    #

    my $link = $path =~ s/(.*auto)/../r;
    $link =~ s/xml/html/g;
    my $report;
    eval{
        $report = XMLin($pathToParse);
    }; 
    if ($@){
        $logger->error("Report file is unreadable. $pathToParse");
        return;
    }
    #
    #Set name for table columns, for following adding values with keys.
    #

    my $TABLE_REPORTS = "\"date\", \"special_id\"";
    my $TABLE_TEST_CASES = "\"platform\", \"version\", \"scenario\", \"start_time\", \"end_time\", \"duration\", \"status\", \"report_id\", \"link\"";
    my $TABLE_STEPS = "\"name\", \"scenario_id\", \"duration\", \"status\", \"test_case_id\"";

    #
    #Report attribures
    #

    my $dateReport = $report->{date};
    my $duration = $report->{duration};
    my $startTime = $report->{startTime};
    my $endTime = $report->{endTime};
    my $productVersion = $report->{productVersion};
    my $platformName = $report->{platformName};
    my $status = $report->{status};
    my $scenarioName = $report->{scenarioName};

    #
    #Check if special day id is exist then use it. Else add report as first report of a day and save it's id.
    #

    my $id;
    my $idExist = workWithTable("select", $specialKey);
    if($idExist eq "") {
        $logger->info("Id $idExist didn't exist in main table. Adding...");
        my $REPORTS = "to_date(\'$dateReport\', \'DD.MM.YYYY\'), \'$specialKey\'";
        $id = workWithTable( "insert", "Reports", $TABLE_REPORTS, $REPORTS );
    } else {
        $logger->info("Id $id exist in main table.");
        $id = $idExist;
    }

    #
    #In table of test cases add information about test scenarios with link on special day id.
    #

    my $TEST_CASES = "\'$platformName\', \'$productVersion\', \'$scenarioName\', \'$startTime\', \'$endTime\', \'$duration\', \'$status\', \'$id\', \'$link\'";
    $id = workWithTable("insert", "TestCases", $TABLE_TEST_CASES, $TEST_CASES );

    #
    #In table of test steps add information about test steps with link on the parent id of test scenario.
    #

    foreach my $step (@{$report->{reportLine}})
    {
        my $stat = $step->{status};
        my $scenId = $step->{scenarioId};
        my $name = $step->{actionName};
        my $stepDuration = calculateDuration( $step->{startTime}, $step->{endTime} );
        if ($scenId eq "") {
            $scenId = "null";
        }
        my $STEPS = "\'$name\', \'$scenId\', \'$stepDuration\', \'$stat\', \'$id\'";
        workWithTable("insert", "Steps", $TABLE_STEPS, $STEPS );
    }
}



sub calculateDuration{
    (my $start,my $end) = @_;
    my $timeformat = "%H:%M:%S";
    my $timeformat2 = "%e %H:%M:%S";
    my $sTime = Time::Piece->strptime( $start, $timeformat );
    my $eTime = Time::Piece->strptime( $end, $timeformat );
    my $result = $eTime - $sTime;
    if($result < 0){
        $sTime = Time::Piece->strptime( "1 ".$start, $timeformat2 );
        $eTime = Time::Piece->strptime( "2 ".$end, $timeformat2 );
        return ($eTime - $sTime);
    } else{
        return $result;
    }
}


sub parsePath{
    my($path, $date) = @_;
    my @temp = split('/', $path);
    my $str = $temp[$#temp - 1];
    my $plat;

    if ($str =~ "ig-") {
        $plat = "ig-";
    } elsif ($str =~ "hw-") {
        $plat = "hw-";
    }
    my $name = substr($str, index($str, $plat));
    my $scenario = substr($name, index($name, '_') + 1 );
    $name = substr($name, index($name, $plat), index($name, '_'));
    $date = substr($date, 0, 8);
    my %map = (
        'date'     => $date,
        'platform' => $name,
        'scenario' => $scenario);
    \ %map;
}
1;
__END__

=head1 NAME

main - Module for parsing test reports and adding information to db.

=head1 DESCRIPTION

This module parse test report and add information in db about it.
Creating specially for autotests.

=head1 USAGE

This module contains three methods.

=head2 parseReportAndAddToDB

Default method, parsing test reports and adding information to db.

Wait for 2 ARGS -

    1) <path>       - path to xml file with report.
    2) <specialKey> - special id include date and feature set.

Example of usage - parseReportAndAddToDB( "report.xml", "16_11_16_gost" );

=head2 calculateDuration

Method for calculate duration between two times.
Return difference between two times.

Wait for 2 ARGS -

    1) <startTime>  - start time in format %H:%M:%S.
    2) <endTime>    - end time in format %H:%M:%S.

Example of usage - calculateDuration( '10:10:10', '11:11:11' )};

=head2 parsePath

Method for get information about failed test from folders name.
Return hash with date, platform and name of scenario.

Wait for 2 ARGS -

    1) <path>       - Path to folder.
    2) <specialKey> - special id include date and feature set.

Example of usage - parsePath("/home/user/16_11_16_hw-100-x1_tunnel/", "16_11_16_gost");
