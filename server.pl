#!/usr/bin/perl
use strict;
use warnings;
no warnings('uninitialized');
use v5.10;
use utf8;
binmode(STDOUT,':utf8');
use threads;
use threads::shared;
use Data::Dumper;
use Time::Piece;
use IO::Socket::INET;
use Log::Log4perl;
use Autostand::main qw(parseReportAndAddToDB);
use Autostand::dbworker qw(workWithTable);
use Autostand::workWithReports qw(createMainReport);

Log::Log4perl::init('/home/user/perl/log4perl.conf');

#
# auto-flush on socket
#
$| = 1;

my (%gost, %west, %ig, @msg, $clientData, $data, $client_socket, $key, $path, $version);
my $lock :shared;
my $host         = "192.168.38.111";
my $logger       = Log::Log4perl->get_logger('autostand');
my $reject       = "Access denied!";
my $waitingTime  = 10800;
my @reportStates = qw( fail complete );
#
#hash with ip addresses of autostands
#
my %AutoStands   = (
    '192.168.38.38'     => '',
    '192.168.38.238'    => '',
    '192.168.38.138'    => '');

#
# creating a listening socket
#
my $socket = IO::Socket::INET->new (
    LocalHost => $host,
    LocalPort => '7777',
    Proto     => 'tcp',
    Listen    => 5,
    Reuse     => 1,
    Blocking  => 0
)or die "$!";

say "server waiting for client connection on port 7777\n";
$logger->info("server waiting for client connection on port 7777\n");
$socket->listen();
$socket->autoflush(1);

while(){
    while($client_socket = $socket->accept()){
        #
        # get information about a newly connected client
        #
        my $client_address = $client_socket->peerhost();
        $logger->info( "connection from $client_address\n" );
        #
        #Check if connection from known ip.
        #
        if ( exists $AutoStands{$client_address} ) {
            $AutoStands{$client_address} = localtime( time );
            $logger->info( "Stand $client_address and time $AutoStands{$client_address}" );
            $client_socket->recv( $clientData, 1024 );
            $logger->info( "received data: $clientData\n" );

            #
            #Client date may contains 2 key words -
            #   planning - as flag for future today tests.
            #   complete - as flag when stand completed tests.
            #in other case client data contains path to report and special key.
            #
            if ($clientData =~ /.*planning.*/) {
                @msg = split( /::/, $clientData );
                $key = $msg[1];
                $logger->info( "Stand $client_address planning for launching tests $key" );

                #
                #Add information about future tests in hast with correct feature set.
                #
                if ($key =~ /.*gost.*/) {
		   lock($lock); 
                   $gost{$client_address} = [ "processing", $key ];
                } elsif ($key =~ /.*west.*/) { 
		   lock($lock); 
                   $west{$client_address} = [ "processing", $key ];
                } elsif ($key =~ /.*ig.*/) {
		   lock($lock); 
                   $ig{$client_address}   = [ "processing", $key ];}

            }elsif ($clientData =~ /.*complete.*/) {
                @msg = split( /::/, $clientData );
                $key = $msg[1];
                $version = $msg[2];
                $logger->info( "Stand $client_address completed tests!" );
                $AutoStands{$client_address} = "";

                #
                #Replace information about completed tests on feature set in hash with correct feature set.
                #
                if ($key =~ /.*gost.*/) {
		    lock($lock); 
                    $gost{$client_address} = [ "complete", $key, $version ];
                } elsif ($key =~ /.*west.*/) { 
		    lock($lock); 
                    $west{$client_address} = [ "complete", $key, $version ];
                } elsif ($key =~ /.*ig.*/) { 
		    lock($lock); 
                    $ig{$client_address} = [ "complete", $key, $version ];}
            } else {
                @msg = split( /::/, $clientData );
                $path = $msg[0];
                $key = $msg[1];
                $logger->info( "PATH is $path and $key is key" );
                parseReportAndAddToDB( $path, $key );
            }
            #
            # write response data to the connected client
            #
            $data = "finish";
            $client_socket->send( $data );
        } else {
            $client_socket->send( $reject ) or $logger->error( "Failed to send information to client $!\n" );
        }
        shutdown( $client_socket, 1 ) or $logger->error( "Failed to shutdown the socket $!\n" );
    }

    #
    # Check if autostand alive.
    #
    foreach(keys %AutoStands) {
        if ($AutoStands{$_} ne "") {
            my $answer = checkWaitingTime( $AutoStands{$_} );
            if ($answer) {
                $logger->error( "Failed to wait answer from $_" );
                if("$_" =~ keys %gost){ 
		    lock($lock); 
                    $gost{$_} = [ "fail" ];}
                elsif ("$_" =~ keys %west){ 
		    lock($lock); 
                    $west{$_} = [ "fail" ];}
                elsif ("$_" =~ keys %ig){ 
		    lock($lock); 
                    $ig{$_} = [ "fail" ];}
                $AutoStands{$_} = "";
                my $dateReport = localtime->strftime( '%H:%M %d.%m.%Y' );
                workWithTable( "insert", "Fails", "\"date\", \"ip\"",
                    "to_timestamp(\'$dateReport\', \'HH24:MI DD.MM.YYYY\'), \'$_\'" );
            }
        }
    }
    #
    # Check if all stands which planning tests on special feature set, completed it.
    #
    checkAndCreateReports(\%gost);
    checkAndCreateReports(\%west);
    checkAndCreateReports(\%ig);
    select(undef, undef, undef, 0.1);
}
$socket->close();

#
# Check if time from last connection from autostand more than constant waitingTime.
#
sub checkWaitingTime{
    my $gotTime = shift;
    my $time = localtime(time) - $gotTime;
    if ( $time > $waitingTime){
        $logger->error("Time diff is $time");
        return 1;
    }
}

#
# Check if all stands which planning tests on special feature set, completed it, and create report.
#
sub checkAndCreateReports{
    my $hash = shift;
    if(0 != scalar( keys %{$hash} )){
        lock($lock);
        my ($finalKey, $finalVersion);
        my $i = 0;
        foreach(values %{$hash} ) {
            $finalKey = @{$_}[1];
            $finalVersion = @{$_}[2];
            $i++ if ("@{$_}[0]" ~~ @reportStates);
        }
        if (scalar( keys %{$hash} ) == $i && 0 != $i) {
            my $tempGost = \%gost;
            my $tempWest = \%west;
            my $tempIg   = \%ig;
            $logger->info("Gost link is $tempGost, West link is $tempWest, Ig link is $tempIg and current link is $hash!");
            $logger->info(Dumper($hash));
            $logger->info("Sending reports for key $finalKey and version $finalVersion!");
            my $idExist = workWithTable("select", $finalKey);
            if('' ne $idExist && '' ne $finalVersion && '' ne $finalKey){
                createMainReport("$idExist","yes", $finalVersion, $finalKey);
            }else{
                $logger->error("One from necessary args undefined - id $idExist, key $finalKey and version $finalVersion!");
            }
            undef %{$hash};
            }
    }
}
