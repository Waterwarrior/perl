package Autostand::workWithReports;
use strict;
use warnings;
no warnings 'uninitialized';
use v5.10;
use utf8;
binmode(STDOUT,':utf8');
use XML::Simple;
use Template;
use POSIX;
use File::Copy;
use Log::Log4perl;
use Autostand::dbworker qw(sqlQueries);
use Autostand::tfs qw(getBugs);
use Exporter qw(import);

our @EXPORT = qw(createMainReport);
my $configXml = '/home/user/perl/server/config.xml';
Log::Log4perl::init('/home/user/perl/log4perl.conf');
my $logger = Log::Log4perl->get_logger('autostand');

sub createMainReport {
    my($id, $send, $version, $featureSet) = @_;
    my $description;
    $logger->info("Start to creating main report id - $id, version is $version,send is $send and feature set is $featureSet");
    my $time = POSIX::strftime( '%H:%M %d.%m.%y', localtime );
    my $dirTime = POSIX::strftime( '%d_%m_%H_%M_%S', localtime );
    my $sharePath = "/autostand/logs/auto/";
    my $file = '/home/user/perl/server/table.tt';
    my $html = '/home/user/perl/server/table.html';

    #
    #Get main information from db and xml.
    #

    my %environment = %{getXmlInfo( $configXml, 'environment' )};
    my %descriptions = %{getXmlInfo( $configXml, 'descriptions' )};
    my %troubles = %{getXmlInfo( $configXml, 'troubles' )};
    my %hash = %{sqlQueries(
        "select platform, status, scenario, link from \"TestCases\" WHERE report_id=($id)", "doublehash" )};
    my %platforms = %{sqlQueries(
        "SELECT platform, version FROM \"TestCases\" WHERE report_id=($id) group by platform, version", "hash" )};
    my @scenarios = @{sqlQueries( "SELECT DISTINCT scenario FROM \"TestCases\" WHERE report_id=($id)",
        "array" )};

    my %bugs = %{getBugs($version)};

    @scenarios = sort(@scenarios);
    my $finalState = "SUCCESS";

    #
    #Add empty cells for generate correct table in html.
    #
    if($featureSet =~ 'gost'){
        $description = '"Base" feature set of HW platform';
    } elsif($featureSet =~ 'west'){
        $description = '"Advanced" feature set of HW platform';
    } elsif($featureSet =~ 'ig'){
        $description = '"IG" feature set of IG platform';
    }
    
    foreach my $sc (@scenarios)
    {
        foreach my $plat (keys %platforms)
        {
            if ($hash{$sc}{$plat}{'state'} eq "") {
                $hash{$sc}{$plat}{'state'} = "tmp";
            } elsif ($hash{$sc}{$plat}{'state'} eq "FAIL")
            {
                $logger->info("Scenario $sc for platform $plat was failed");
                $finalState = "FAIL";
            }
        }
    }

    my $head = "$time Test report for $description. Total status - $finalState";

    #
    #Add general options for template.
    #

    my $vars = {
        head         => $head,
        platforms    => \%platforms,
        list         => \%hash,
        environment  => \%environment,
        descriptions => \%descriptions,
        troubles     => \%troubles,
        bugs         => \%bugs,
        title        => "Full report"
    };

    my $template = Template->new( {
            ENCODING => 'utf8',
            RELATIVE => 1,
            ABSOLUTE => 1
        } );

    #
    #Use template for creating html.
    #

    $template->process( $file, $vars, $html, { binmode => ':encoding(UTF-8)' } )
        || die "Template process failed: ", $template->error(),
        "\n";

    my $reportPath = "${sharePath}${dirTime}_mainReport";
    $logger->info("Report path is $reportPath");
    mkdir "$reportPath";
    copy $html, "${reportPath}/mainReport.html";
    sendMail("${reportPath}/mainReport.html", $head, $version) if $send eq "yes";
}

sub getXmlInfo{
    my($path, $tag) = @_;
    my %result;
    my $data = XMLin($path);
    foreach my $name (keys %{$data->{$tag}->{'string'}}) {
        $result{$name} = $data->{$tag}->{'string'}->{$name}->{value};
    }
    \%result;
}

sub sendMail{
    my($path, $head, $version) = @_;
    use MIME::Lite;
    $logger->info("Send mail with path to report $path and title is $head");
    my $settings = XMLin($configXml);
    my $to = $settings->{'mail'}->{'to'};
    my $bcc = $settings->{'mail'}->{'bcc'};
    my $from = $settings->{'mail'}->{'from'};
    my $server = $settings->{'mail'}->{'server'};
    my $port = $settings->{'mail'}->{'port'};
    my $subject = $head;
    my $cc;
    if("4.1.10" =~ $version){
        $cc = $settings->{'mail'}->{'ccig'};
    } else{
        $cc = $settings->{'mail'}->{'cc'};
    }
    my $dir = $path =~ s/(.*auto.)//r;
    my $message = "$head<br>
    <a href=\"http://wiki.infotecs.int/pages/viewpage.action?pageId=18814338\">Tests schedule</a>
    <br>
    <a href=\"\\\\172.21.172.37\\store\\logs\\auto\\$dir\">Full report</a>
    <br>
    <a href=\"ftp://172.21.172.37/logs/auto/$dir\">Linux way</a>
    <br>
    <a href=\"ftp://192.168.165.37/logs/auto/$dir\">Test Net</a>
    ";

    #
    #Add general options for letter.
    #

    my $msg = MIME::Lite->new(
        From     => $from,
        To       => $to,
        Cc       => $cc,
        Bcc      => $bcc,
        Subject  => $subject,
        Type     => 'multipart/mixed'
    );

    #
    #Add your text message.
    #

    $msg->attach(Type         => 'text/html',
                 Data         => $message
    );

    #
    # Specify your file as attachement.
    #

    $msg->attach(
        Path        => $path,
        Disposition => 'attachment'
    );
    $msg->send('smtp', "${server}:${port}", AuthUser=>'infotecs-nt\\autotesting', AuthPass=>"autotesting");
}
1;
__END__

=head1 NAME

workWithReports - Module for creating main report of a day.

=head1 DESCRIPTION

This module create main report of a day, according to information about tests from db.
Creating specially for autotests.

=head1 USAGE

This module contains three methods.

=head2 createMainReport

Default method, create a table of results and use platforms name as columns and scenario name as a rows.

Wait for 3 ARGS -

    1) <id>       - special id from db that have all tests which run in one day with same feature set.
    2) <send>     - Send or no report on mail for interests.
    3) <version>  - Version of tested platforms. Necessary ARG if send equal yes

Example of usage - createMainReport( "11", "yes", "4.2.0" );

=head2 getXmlInfo

Method for collect information from xml file and return result as a hash.
Usage for getting environment, scenario descriptions and etc.

Wait for 2 ARGS -

    1) <path>     - Path for xml file.
    2) <tag>      - Tag for collected information.

Example of usage - getXmlInfo( 'config.xml', 'environment' )};

=head2 sendMail

Method for sending information on mail for interests.

Wait for 3 ARGS -

    1) <path>    - Path for html file.
    2) <subject> - Head of letter.
    3) <version> - Version of tested platforms.

Example of usage - sendMail("mainReport.html", "full report", "4.2.0");
