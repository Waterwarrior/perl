package Autostand::tfs;

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');
use v5.10;

use LWP::UserAgent;
use HTTP::Headers;
use HTTP::Request::Common;
use HTTP::Cookies;
use Authen::NTLM;
use URI::Escape;
use JSON;
use Log::Log4perl;
use Exporter qw(import);

Log::Log4perl::init('/home/user/perl/log4perl.conf');
my $logger = Log::Log4perl->get_logger('autostand');

our @EXPORT = qw(getBugs);

sub getBugs {
    $|++;

    my $version = shift;
    $logger->info("Try to gettiong bugs for version $version");
    my $Options = {
        user          => "autotesting",
        password      => "autotesting",
        domain        => "infotecs-nt",
        AuthMethod    => "NTLM",
        RequestMethod => "GET",
    };

    my $cookie_jar = HTTP::Cookies->new;
    my $browser = LWP::UserAgent->new(
        agent      => 'Mozilla/5.0 (compatible; MSIE 6.0; Windows NT 5.0)',
        keep_alive => '1'
    );

    #
    # First stage of NTLM authentication
    #

    my $url = "http://10.0.4.88:8080/tfs/";

    ntlm_domain( $Options->{'domain'} );
    ntlm_user( $Options->{'user'} );
    ntlm_password( $Options->{'password'} );

    my $Authorization = Authen::NTLM::ntlm();

    my $header = HTTP::Headers->new(
        Content_Type       => 'text/html',
        'WWW-Authenticate' => $Options->{'AuthMethod'}
    );

    $header->header( 'Authorization' => "NTLM $Authorization" );
    my $request = HTTP::Request->new( $Options->{'RequestMethod'} => $url, $header );
    my $res = $browser->request( $request );

    #
    # Second stage of authentication
    #

    my $Challenge = $res->header( 'WWW-Authenticate' );
    $Challenge =~ s/^NTLM //g;
    $Authorization = Authen::NTLM::ntlm( $Challenge );
    $header->header( 'Authorization' => "NTLM $Authorization" );
    $request = HTTP::Request->new( $Options->{'RequestMethod'} => $url, $header );
    $res = $browser->request( $request );
    $cookie_jar->extract_cookies( $res );
    #
    # ntlm needs to be resetted after second stage
    #
    ntlm_reset();

    #
    # Get result of query from tfs
    #
    $url = "http://10.0.4.88:8080/tfs/infotecstc1/PRG_VPN_Linux/_api/_wit/query?__v=5";

    my $http_post_body =
        "SELECT [System.Id],[System.Title],[Microsoft.VSTS.CMMI.HowFound] FROM WorkItems ".
        "WHERE [System.TeamProject] = \@project AND [System.WorkItemType] = 'Bug' ".
        "AND [Microsoft.VSTS.CMMI.HowFound] CONTAINS 'autostand' ".
        "AND [System.IterationPath] UNDER 'PRG_VPN_Linux' AND [System.State] <> 'Closed' ".
        "AND [Microsoft.VSTS.Common.Priority] <= 2 AND [Microsoft.VSTS.Build.FoundIn] CONTAINS '$version'";

    $http_post_body = "wiql=" . uri_escape( $http_post_body );
    my $req = HTTP::Request->new( POST => $url );
    $req->content_type( 'application/x-www-form-urlencoded; charset=UTF-8' );
    $req->content( $http_post_body );
    $browser->cookie_jar( $cookie_jar );
    $res = $browser->request( $req );


    #
    # Parse result of query, and return hash with it.
    #

    my $json = JSON->new->utf8->allow_nonref;
    my %hash;

    if ($res->is_success()) {
        my $perlData = $json->decode( $res->content() );

        foreach my $bug (@{ $perlData->{payload}->{rows} })
        {
            my @random = @{ $bug };
            my $testScenario = $random[2] =~ s/autostand//ir;
            $hash{$random[0]} = [ $random[1], $testScenario ];
        }
        $logger->info("Operation getting list of bugs from tfs was successful.");
    }
    else {
        $hash{" "} = [ "", "Error - Failed to connect to tfs!" ];
        $logger->error("Failed to get list of bugs from tfs.");
        $logger->error($res->status_line);
    }
    \ %hash;
}
1;
__END__

=head1 NAME

tfs - Module for work with TFS.

=head1 DESCRIPTION

This module create connection to TFS with ntlm authentication, make query, and return hash with collected information.

=head1 USAGE

This module contains just one method for getting actual bugs.

=head2 getBugs

Example of usage - getBugs('4.2.0');

Wait for just one AGRS with product version, after that returned all bugs that contains -

    1) This version.
    2) Priority 2 or higher.
    3) State - not closed.
    4) The word 'autostand' in the field 'How found'.
