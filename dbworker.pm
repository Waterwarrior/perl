package Autostand::dbworker;
use strict;
use warnings;
no warnings 'experimental::smartmatch';
use v5.10;
use utf8;
binmode(STDOUT,':utf8');
use Log::Log4perl;
use DBI;
use Exporter qw(import);
Log::Log4perl::init('/home/user/perl/log4perl.conf');

#
#Add options for connection to db.
#

my $dbname   = "Main";
my $username = "postgres";
my $password = "aaaaaa";
my $dbhost   = "127.0.0.1";
my $dbport   = "5432";
my $logger   = Log::Log4perl->get_logger('autostand');

our @EXPORT_OK = qw(workWithTable sqlQueries);

sub workWithTable {
    my $dbh = DBI->connect("dbi:Pg:dbname=$dbname;host=$dbhost;port=$dbport;","$username","$password",
        {PrintError => 0}) or say "Could not established connect with database $dbname";
    my ($param1, $param2, $param3, $param4) = @_;
    my $query;
    if($param1 eq "insert") {
        $query = "insert into \"$param2\" ($param3) values ($param4) returning id";
    }else{
        $query = "select \"id\" from \"Reports\" where \"special_id\"= \'$param2\'";
    }
    my $sth = $dbh->prepare( $query ) or $logger->error("Failed to prepared query! $!");
    my $rv = $sth->execute() or $logger->error("Failed to executed query! $!");

    if (!defined $rv) {
        $logger->error("After '$query' was executed was found error: ".$dbh->errstr." $!\n");
        exit( 0 );
    }

    my $id = $sth->fetchrow_array() or $logger->error("Failed to get id! $!");

    $sth->finish() or $logger->error("Failed to finished work with database! $!");

    $dbh->disconnect() or $logger->error("Failed to disconnected from database! $!");
    return $id;
}

sub sqlQueries{
    my $db = DBI->connect("dbi:Pg:dbname=$dbname;host=$dbhost;port=$dbport;","$username","$password",
        {PrintError => 0}) or say "Could not established connect with database $dbname";
    my ($query, $answer) = @_;
    my $mytable_output = $db->prepare($query);
    $mytable_output->execute();

    given ($answer)
    {
        when ('doublehash') {
            my %hash;
            while(my ($platform, $status, $scenario, $link) = $mytable_output->fetchrow_array){
                $hash{$scenario}{$platform}{'state'} = $status;
                $hash{$scenario}{$platform}{'link'} = $link;
            }
            return \%hash;
        }
        when ('array'){
            my @array;
            while(my $row = $mytable_output->fetchrow_array){
                push @array, $row;
            }
            return \@array;
        }
        when ('hash'){
            my %hash;
            while(my ($key, $value) = $mytable_output->fetchrow_array){
                $hash{$key} = $value;
            }
            return \%hash;
        }
    }

}
1;
__END__

=head1 NAME

dbworker - Module for work with database.

=head1 DESCRIPTION

This module make queries to db for select or insert information.
Creating specially for autotests.

=head1 USAGE

This module contains two methods.

=head2 workWithTable

Usually use for insert new information about test result to db, and return id for linked table.

Wait for 4 ARGS -

    1) <operation>  - insert or select(select just for one case).
    2) <table name> - Name of table.
    3) <keys>       - Keys for table.
    4) <values>     - Values for keys.

Example of usage - workWithTable( "insert", "Reports", "\"date\", \"special_id\"", "to_date(\'$dateReport\', \'DD.MM.YYYY\'), \'$specialKey\'" );
It's equal for sql query - INSERT into "Reports" ("date", "special_id") values (to_date('$dateReport', 'DD.MM.YYYY'), '$specialKey') returning id

=head2 sqlQueries

Method for collect information from db and return link on hash or array with result of operation.

Wait for 2 ARGS -

    1) <query>  - Full query like into db.
    2) <answer> - Expected information in answer, like hash or array.

Example of usage - sqlQueries( "SELECT DISTINCT scenario FROM \"TestCases\" WHERE report_id=($id)", "array" );
